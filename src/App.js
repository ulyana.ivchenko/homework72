import React, {Component} from 'react';
import Number from './Number/Number';
import './App.css';


function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function compareValues(key, order = 'asc') {
    return function innerSort(a, b) {
        const varA = a[key];
        const varB = b[key];
        let comparison = 0;
        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }
        return comparison;
    };
}


class App extends Component {

    state = {
        numbers: [
            {number: 5},
            {number: 11},
            {number: 16},
            {number: 22},
            {number: 36}
        ]
    };

    setNewNumbers = () => {
        const numbers = this.state.numbers.map((number) => {
            return {
                number: randomIntFromInterval(5, 36)
            };
        });
        const distinctNumbers = [...new Set(numbers.map(x => x.number))];
        const a = numbers.length;
        const b = distinctNumbers.length;
        if (a === b) {
            numbers.sort(compareValues('number'));
            this.setState({numbers});
        }
        else {
            const numbers = this.state.numbers.map((number) => {
                return {
                    number: randomIntFromInterval(5, 36)
                };
            });
            numbers.sort(compareValues('number'));
            this.setState({numbers});
        }
    };

    render() {
        return (
            <div className="App">
                <Number number={this.state.numbers[0].number}/>
                <Number number={this.state.numbers[1].number}/>
                <Number number={this.state.numbers[2].number}/>
                <Number number={this.state.numbers[3].number}/>
                <Number number={this.state.numbers[4].number}/>
                <div>
                    <button onClick={this.setNewNumbers}>New Numbers</button>
                </div>
            </div>
        )
    }
}

export default App;
