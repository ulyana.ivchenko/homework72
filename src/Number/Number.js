import React from 'react';

const Number = props => {
    return (
        <div className="number">
            <h5>{props.number}</h5>
        </div>
    );
};

export default Number;